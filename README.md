# Overview
My I/O library, aka "mio." A python module composed of general purpose utilities for hacking together quick scripts. 

### Installation
* Test local installation with `source test.sh`
* install from [PyPi](https://pypi.org/project/rm3-mio/) using `pip install rm3-mio` 

### Modules
* Platform (system threading, shell commands, popups)
* Storage (reading / writing files and directories)
* Struct (working with data structures)
* Terminal (inputting / outputting text on the console)
* Web (fetch resources from the internet)

### PyPi Instructions
1. Make an account [here](https://pypi.org/account/register/)
2. Generate API token [here](https://pypi.org/manage/account/token/)
3. Increment version `setup.py`
4. Upload project `source upload.sh`
5. Implement a [CI pipeline](https://docs.gitlab.com/ee/user/packages/pypi_repository/)

# Author
| Shahrose Kasim |             |
|----------------|-------------|
|*[shahros3@gmail.com](mailto:shahros3@gmail.com)*|[shahrose.com](http://shahrose.com)|
|*[rosemaster3000@gmail.com](mailto:rosemaster3000@gmail.com)*|[florasoft.live](https://florasoft.live) |
|*[RoseMaster#3000](https://discordapp.com/users/122224041296789508)*|[discord.com](https://discord.com/)|
